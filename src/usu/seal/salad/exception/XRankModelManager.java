package usu.seal.salad.exception;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.opencsv.CSVReader;
import org.apache.commons.lang3.tuple.Pair;
import usu.seal.salad.util.Constants;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

public class XRankModelManager {
    private BiMap<String, Integer> objectBiMap;
    private BiMap<String, Integer> methodBiMap;
    private Map<Integer, Integer> n_m;
    private Map<Integer, Integer> n_e;
    private Map<Pair<Integer, Integer>, Integer> n_me;

    public XRankModelManager() throws IOException {
        objectBiMap = HashBiMap.create();
        methodBiMap = HashBiMap.create();
        n_m = new HashMap<>();
        n_e = new HashMap<>();
        n_me = new HashMap<>();
        loadModel();
    }

    private void loadModel() throws IOException {
        InputStream resourceAsStream = this.getClass().getResourceAsStream(Constants.OBJECTS_FILE);
        CSVReader objectsCsvReader = new CSVReader(new InputStreamReader(resourceAsStream));
        String[] readEntry;
        while ((readEntry = objectsCsvReader.readNext()) != null) {
            objectBiMap.put(readEntry[0], Integer.parseInt(readEntry[1]));
        }
        objectsCsvReader.close();

        resourceAsStream = this.getClass().getResourceAsStream(Constants.METHOD_NAMES_FILE);
        CSVReader methodsCsvReader = new CSVReader(new InputStreamReader(resourceAsStream));
        while ((readEntry = methodsCsvReader.readNext()) != null) {
            methodBiMap.put(readEntry[0], Integer.parseInt(readEntry[1]));
        }
        methodsCsvReader.close();

        resourceAsStream = this.getClass().getResourceAsStream(Constants.N_M);
        CSVReader nmCSVReader = new CSVReader(new InputStreamReader(resourceAsStream));
        while ((readEntry = nmCSVReader.readNext()) != null) {
            n_m.put(Integer.parseInt(readEntry[0]), Integer.parseInt(readEntry[1]));
        }
        nmCSVReader.close();

        resourceAsStream = this.getClass().getResourceAsStream(Constants.N_E);
        CSVReader neCSVReader = new CSVReader(new InputStreamReader(resourceAsStream));
        while ((readEntry = neCSVReader.readNext()) != null) {
            n_e.put(Integer.parseInt(readEntry[0]), Integer.parseInt(readEntry[1]));
        }
        neCSVReader.close();

        resourceAsStream = this.getClass().getResourceAsStream(Constants.N_ME);
        CSVReader nmeCSVReader = new CSVReader(new InputStreamReader(resourceAsStream));
        while ((readEntry = nmeCSVReader.readNext()) != null) {
            Pair<Integer, Integer> me = Pair.of(Integer.parseInt(readEntry[0]), Integer.parseInt(readEntry[1]));
            n_me.put(me, Integer.parseInt(readEntry[2]));
        }
        nmeCSVReader.close();
    }

    private List<Pair<Integer, Double>> predict(List<Integer> testObject) {
        List<Pair<Integer, Double>> result = new ArrayList<Pair<Integer, Double>>();
        for (Integer methodId : testObject) {
            if (!n_m.containsKey(methodId)) return null;
        }
        for (Map.Entry<Integer, Integer> entrySet : n_e.entrySet()) {
            int exceptionId = entrySet.getKey();
            int exceptionCount = entrySet.getValue();
            int methodCount;
            int bothCount;
            double score = 1;
            double mu;
            for (Integer methodId : testObject) {
                Pair<Integer, Integer> pair = Pair.of(methodId, exceptionId);
                if (!n_me.containsKey(pair)) {
                    bothCount = 0;
                } else {
                    bothCount = n_me.get(pair);
                }
                methodCount = n_m.get(methodId);
                mu = (double)bothCount/(methodCount+exceptionCount-bothCount);
                score = score * (1-mu);
            }
            score = 1-score;
            result.add(Pair.of(exceptionId,score));
        }
        Collections.shuffle(result);
        Collections.sort(result, new Comparator<Pair<Integer, Double>>() {
            @Override
            public int compare(Pair<Integer, Double> o1,
                               Pair<Integer, Double> o2) {
                return -Double.compare(o1.getRight(), o2.getRight());
            }
        });
        return result;
    }

    public List<Pair<String, Double>> predictExceptions(List<String> methods) {
        System.out.println(methodBiMap.size());
        List<Pair<String, Double>> result = new ArrayList<>();
        List<Integer> methodIds = new ArrayList<>();
        for (String method : methods) {
            methodIds.add(methodBiMap.get(method));
        }
        System.out.println(methodIds);

        List<Pair<Integer, Double>> predict = predict(methodIds);
        for (Pair<Integer, Double> integerDoublePair : predict) {
            Pair<String, Double> pair = Pair.of(objectBiMap.inverse().get(integerDoublePair.getLeft()), integerDoublePair.getRight());
            result.add(pair);
        }
        return result;
    }

    public double calculateExceptionRisk(String method) {
        Integer methodID = methodBiMap.get(method);
        if (methodID == null) return 0;
        double risk = 0.0;

        for (Map.Entry<Integer, Integer> exceptionEntry : n_e.entrySet()) {
            int exceptionId = exceptionEntry.getKey();
            int exceptionCount = exceptionEntry.getValue();
            Pair<Integer, Integer> pair = Pair.of(methodID, exceptionId);
            int bothCount;
            if (!n_me.containsKey(pair)) {
                bothCount = 0;
            } else {
                bothCount = n_me.get(pair);
            }
            int methodCount = n_m.get(methodID);
            double methodCountDouble = (double) methodCount;
            risk += (double)bothCount/methodCountDouble;
        }



        return risk;
    }
}
