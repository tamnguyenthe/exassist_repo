package usu.seal.salad.exception;

import java.io.Serializable;
import java.util.Arrays;

public class ExceptionExample implements Serializable {
    private static final long serialVersionUID = -6312891216423388670L;
    private String[] types;
    private String[] methodNames;
    private String[] exceptionTypes;
    private String handlingCode;

    public ExceptionExample() {}

    public String[] getTypes() {
        return types;
    }

    public void setTypes(String[] types) {
        this.types = types;
    }

    public String[] getMethodNames() {
        return methodNames;
    }

    public void setMethodNames(String[] methodNames) {
        this.methodNames = methodNames;
    }

    public String getHandlingCode() {
        return handlingCode;
    }

    public void setHandlingCode(String handlingCode) {
        this.handlingCode = handlingCode;
    }

    @Override
    public String toString() {
        return "ExceptionExample{" +
                "types=" + Arrays.toString(types) +
                ", methodNames=" + Arrays.toString(methodNames) +
                ", exceptionTypes='" + Arrays.toString(exceptionTypes) + '\'' +
                ", handlingCode='" + handlingCode + '\'' +
                '}';
    }

    public String[] getExceptionTypes() {
        return exceptionTypes;
    }

    public void setExceptionTypes(String[] exceptionTypes) {
        this.exceptionTypes = exceptionTypes;
    }
}
