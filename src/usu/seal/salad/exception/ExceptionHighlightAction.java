package usu.seal.salad.exception;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.editor.Editor;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.PsiMethodCallExpression;
import com.intellij.psi.util.PsiTreeUtil;
import org.jaxen.function.NamespaceUriFunction;
import usu.seal.salad.cfg.SourceCodeCFG;
import usu.seal.salad.cfg.SourceCodeCFGCreator;
import usu.seal.salad.cfg.node.SMethodNode;
import usu.seal.salad.cfg.node.SNode;
import usu.seal.salad.cfg.visitor.HighlightExpressionVisitor;
import usu.seal.salad.util.AndroidAPIUtils;

import java.io.IOException;
import java.util.Collection;

public class ExceptionHighlightAction extends AnAction {

    private static XRankModelManager manager = null;

    public static XRankModelManager getXRankModelManager() {
        if (manager == null) {
            try {
                manager = new XRankModelManager();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return manager;
    }

    @Override
    public void actionPerformed(AnActionEvent e) {
        Editor editor = e.getData(CommonDataKeys.EDITOR);
        PsiFile psiFile = e.getData(CommonDataKeys.PSI_FILE);
        int offset = editor.getCaretModel().getOffset();
        PsiElement currentElement = psiFile.findElementAt(offset);
        PsiElement psiMethod = PsiTreeUtil.getParentOfType(currentElement, PsiMethod.class);
        if (psiMethod == null) return;

        SourceCodeCFGCreator sourceCodeCFGCreator = new SourceCodeCFGCreator((PsiMethod) psiMethod);
        sourceCodeCFGCreator.buildCFG();
        SourceCodeCFG cfg = sourceCodeCFGCreator.getCfgParameters().getCfg();

        Collection<PsiMethodCallExpression> childrenOfType = PsiTreeUtil.findChildrenOfType(psiMethod, PsiMethodCallExpression.class);

        for (PsiMethodCallExpression psiMethodCallExpression : childrenOfType) {
            HighlightExpressionVisitor visitor = new HighlightExpressionVisitor(editor);
            psiMethodCallExpression.accept(visitor);
        }
        for (SNode sNode : cfg.getNodes()) {
            if (sNode instanceof SMethodNode) {
                SMethodNode methodNode = (SMethodNode) sNode;
                if (AndroidAPIUtils.isAndroidAPI(methodNode)) {
                    String method = methodNode.getSignature().toDalvikString();
                    double risk = getXRankModelManager().calculateExceptionRisk(method);
                    HighlightExpressionVisitor visitor = new HighlightExpressionVisitor(editor, risk);


                    currentElement = psiFile.findElementAt(methodNode.getStartOffset());
                    PsiMethodCallExpression methodCallExpression = null;
                    try {
                        methodCallExpression = PsiTreeUtil.getParentOfType(currentElement, PsiMethodCallExpression.class);
                    } catch (NullPointerException exception) {
                        exception.printStackTrace();
                    }
                    if (methodCallExpression != null) {
                        methodCallExpression.accept(visitor);
                    }
                }
            }
        }
    }
}
