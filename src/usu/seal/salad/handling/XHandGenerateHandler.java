package usu.seal.salad.handling;

import com.intellij.codeInsight.CodeInsightActionHandler;
import com.intellij.codeInsight.actions.ReformatCodeAction;
import com.intellij.codeInsight.hint.HintManager;
import com.intellij.ide.DataManager;
import com.intellij.openapi.actionSystem.*;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.EditorModificationUtil;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.popup.JBPopupFactory;
import com.intellij.openapi.util.TextRange;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.*;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.util.containers.ContainerUtil;
import com.intellij.util.ui.UIUtil;
import org.apache.commons.lang3.tuple.Pair;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import usu.seal.salad.cfg.SourceCodeCFG;
import usu.seal.salad.cfg.SourceCodeCFGCreator;
import usu.seal.salad.cfg.node.SMethodNode;
import usu.seal.salad.cfg.node.SMethodSignature;
import usu.seal.salad.cfg.node.SNode;
import usu.seal.salad.cfg.node.SObjectNode;
import usu.seal.salad.exception.ExceptionExample;
import usu.seal.salad.exception.XRankModelManager;
import usu.seal.salad.util.AndroidAPIUtils;
import usu.seal.salad.util.TypeUtil;

import java.io.IOException;
import java.util.*;

public class XHandGenerateHandler implements CodeInsightActionHandler {

    private static final String CHOOSER_TITLE = "Handling Sequences";

    private static XHandModelManager manager = null;
    public static XHandModelManager getXHandModelManager() {
        if (manager == null) {
            try {
                manager = new XHandModelManager();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return manager;
    }

    @Override
    public boolean startInWriteAction() {
        return true;
    }

    @Override
    public void invoke(@NotNull Project project, @NotNull Editor editor, @NotNull PsiFile file) {
        if (!EditorModificationUtil.checkModificationAllowed(editor)) return;
        if (file instanceof PsiCompiledElement) {
            HintManager.getInstance().showErrorHint(editor, "Can't modify decompiled code");
            return;
        }

        List<AnAction> applicable = buildXHandGeneratorActions(project, editor, file);
        if (applicable != null) {
            WriteCommandAction.runWriteCommandAction(project, new Runnable() {
                @Override
                public void run() {
                    XHandGeneratorAction action = (XHandGeneratorAction)applicable.get(0);
                    generateHandlingSequences(project, editor, action.getMyGenerator());
                    final String actionId = IdeActions.ACTION_EDITOR_REFORMAT;
                    ArrayList<VirtualFile> files = ContainerUtil.newArrayList(file.getVirtualFile());
                    AdditionalEventInfo eventInfo = new AdditionalEventInfo();
                    eventInfo.setEditor(editor);
                    ReformatCodeAction reformatCodeAction = (ReformatCodeAction) ActionManager.getInstance().getAction(actionId);
                    reformatCodeAction.actionPerformed(AnActionEvent.createFromAnAction(action, null, "", dataId -> {
                        if (CommonDataKeys.VIRTUAL_FILE_ARRAY.is(dataId)) return files.toArray(VirtualFile.EMPTY_ARRAY);
                        if (CommonDataKeys.PROJECT.is(dataId)) return project;
                        if (CommonDataKeys.EDITOR.is(dataId)) return eventInfo.getEditor();
                        if (LangDataKeys.MODULE_CONTEXT.is(dataId)) return eventInfo.getModule();
                        if (CommonDataKeys.PSI_ELEMENT.is(dataId)) return eventInfo.getElement();
                        return null;
                    }));
                }
            });
        }
        else if (!ApplicationManager.getApplication().isUnitTestMode()) {
            HintManager.getInstance().showErrorHint(editor, "Couldn't find Surround With variants applicable to the current context");
        }
    }

    private List<AnAction> buildXHandGeneratorActions(Project project, Editor editor, PsiFile file) {
        int offset = editor.getCaretModel().getOffset();
        PsiElement currentElement = file.findElementAt(offset);
        PsiElement catchStatement = PsiTreeUtil.getParentOfType(currentElement, PsiCatchSection.class);
        if (catchStatement == null) return null;
        PsiElement tryStatement = PsiTreeUtil.getParentOfType(currentElement, PsiTryStatement.class);
        if (tryStatement == null) return null;
        PsiTryStatement tryStatement1 = (PsiTryStatement)tryStatement;
        PsiCodeBlock tryBlock = tryStatement1.getTryBlock();
        TextRange textRange = tryBlock.getTextRange();
        PsiElement psiMethod = PsiTreeUtil.getParentOfType(currentElement, PsiMethod.class);
        SourceCodeCFGCreator sourceCodeCFGCreator = new SourceCodeCFGCreator((PsiMethod)psiMethod);
        sourceCodeCFGCreator.buildCFG();
        SourceCodeCFG cfg = sourceCodeCFGCreator.getCfgParameters().getCfg();


        Set<String> objectSet = new HashSet<>();
        Set<String> methodSet = new HashSet<>();

        // TODO: extract methods from cfg
        Map<Integer, SObjectNode> objectNodeMap = new HashMap<>();
        for (SNode sNode : cfg.getNodes()) {
            if (sNode instanceof SMethodNode) {
                SMethodNode methodNode = (SMethodNode)sNode;
                if (textRange.contains(methodNode.getStartOffset())) {

                    if (AndroidAPIUtils.isAndroidAPI(methodNode)) {
                        objectSet.add(TypeUtil.convertClassStringToDalvik(methodNode.getSignature().getContainingClass()));
                        System.out.println(methodNode.getSignature().toDalvikString());
                        methodSet.add(methodNode.getSignature().toDalvikString());
                    }

                    for (SNode node : methodNode.getDataEdges()) {
                        if (node instanceof SObjectNode) {
                            SObjectNode objectNode = (SObjectNode) node;
                            if (AndroidAPIUtils.isAndroidAPI(objectNode)) {
                                if (!objectNodeMap.containsKey(objectNode.getId())) {
                                    objectNodeMap.put(objectNode.getId(), objectNode);
                                }
                                objectSet.add(TypeUtil.convertClassStringToDalvik(objectNode.getType()));
                            }
                        }
                    }
                    for (SNode node : methodNode.getDataBackEdges()) {
                        if (node instanceof SObjectNode) {
                            SObjectNode objectNode = (SObjectNode) node;
                            if (AndroidAPIUtils.isAndroidAPI(objectNode)) {
                                if (!objectNodeMap.containsKey(objectNode.getId())) {
                                    objectNodeMap.put(objectNode.getId(), objectNode);
                                }
                                objectSet.add(TypeUtil.convertClassStringToDalvik(objectNode.getType()));
                            }
                        }
                    }
                }
            }
        }


        String[] exceptionTypes = new String[1];
        int i =0;
        exceptionTypes[i++] = ((PsiCatchSection) catchStatement).getCatchType().getCanonicalText();

        String[] objectList = new String[objectSet.size()];
        i = 0;
        for (String objectType : objectSet) {
            objectList[i++] = objectType;
        }
        String[] methodList = new String[methodSet.size()];
        i = 0;
        for (String methodString : methodSet) {
            methodList[i++] = methodString;
        }
        ExceptionExample exceptionExample = null;
        if (objectList.length > 0 && methodList.length > 0 && exceptionTypes.length > 0) {

            exceptionExample = new ExceptionExample();
            exceptionExample.setTypes(objectList);
            exceptionExample.setMethodNames(methodList);
            exceptionExample.setHandlingCode(null);
            exceptionExample.setExceptionTypes(exceptionTypes);

        }

        System.out.println(exceptionExample);


        System.out.println("*******************");

        rankAndSaveExceptionExamples(exceptionExample);

//        System.out.println(getXHandModelManager().getExceptionExamples());

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("// generated by ExAssist\n");
        for (Map.Entry<Integer, SObjectNode> entry : objectNodeMap.entrySet()) {
            if (textRange.contains(entry.getValue().getOffset())) continue;
            String dalvikMethod = getXHandModelManager().getHandlingMethodMap().get(TypeUtil.convertClassStringToDalvik(entry.getValue().getType()));
            if (entry.getValue().getDataEdges().size() > 0 && dalvikMethod != null) {
                stringBuilder.append("if (");
                stringBuilder.append(entry.getValue().getIdentifier());
                stringBuilder.append(" != null) {\n");
                SMethodSignature sMethodSignature = TypeUtil.convertMethodDalvikString(dalvikMethod);
                stringBuilder.append(entry.getValue().getIdentifier() + "." + sMethodSignature.getName() + "();\n");
                stringBuilder.append("}\n");
            } else {
                stringBuilder.append(entry.getValue().getIdentifier());
                stringBuilder.append(" = null;\n");
            }
        }
        PsiMethod method = (PsiMethod) psiMethod;
        System.out.println(method.getReturnType().getCanonicalText());
        if (!method.getReturnType().getCanonicalText().contains("void")) {
            stringBuilder.append("return _DEFAULT_VALUE;\n");
        }


        XHandGeneratorAction action = new XHandGeneratorAction(new XHandGenerator(stringBuilder.toString()), project, editor, '1');
        List<AnAction> applicable = new ArrayList<>();
        applicable.add(action);
        return applicable;
    }

    private void rankAndSaveExceptionExamples(ExceptionExample query) {
        List<Pair<Double, ExceptionExample>> rankList = new ArrayList<>();
        for (ExceptionExample example : getXHandModelManager().getExceptionExamples()) {
            Pair<Double, ExceptionExample> entry = Pair.of(computeScore(query, example), example);
            rankList.add(entry);
        }
        Collections.sort(rankList, new Comparator<Pair<Double, ExceptionExample>>() {
            @Override
            public int compare(Pair<Double, ExceptionExample> o1, Pair<Double, ExceptionExample> o2) {
                return -Double.compare(o1.getLeft(), o2.getLeft());
            }
        });
        int size = rankList.size() > 5 ? 5 : rankList.size();
        for (int i = 0; i < size; i++) {
            System.out.println("Score:" + rankList.get(i).getLeft());
            System.out.println();
            System.out.println(rankList.get(i).getRight().getHandlingCode());
            System.out.println("------------------------------------------------");
        }
    }

    private Double computeScore(ExceptionExample query, ExceptionExample example) {
        double f1 = 0.0;
        double f2 = 0.0;

        Set<String> objectSet1 = createSetFromArray(query.getTypes());
        Set<String> objectSet2 = createSetFromArray(example.getTypes());

        objectSet1.retainAll(objectSet2);

        f1 += objectSet1.size();
        f2 += objectSet1.size() * 1.0;

        Set<String> methodSet1 = createSetFromArray(query.getMethodNames());
        Set<String> methodSet2 = createSetFromArray(query.getMethodNames());

        methodSet1.retainAll(methodSet2);

        f1 += methodSet1.size();
        f2 += methodSet1.size() * 1.0;

        Set<String> exSet1 = createSetFromArray(query.getExceptionTypes());
        Set<String> exSet2 = createSetFromArray(query.getExceptionTypes());

        exSet1.retainAll(exSet2);

        f1 += exSet1.size();
        f2 += exSet2.size() * 1.0;

        f1 = f1/(query.getExceptionTypes().length + query.getMethodNames().length + query.getExceptionTypes().length);

        return f1*f2;
    }

    private Set<String> createSetFromArray(String[] arr) {
        Set<String> set = new HashSet<>();
        for (String s : arr) {
            set.add(s);
        }
        return set;
    }

    private static void showPopup(Editor editor, List<AnAction> applicable) {
        DataContext context = DataManager.getInstance().getDataContext(editor.getContentComponent());
        JBPopupFactory.ActionSelectionAid mnemonics = JBPopupFactory.ActionSelectionAid.MNEMONICS;
        DefaultActionGroup group = new DefaultActionGroup(applicable.toArray(new AnAction[applicable.size()]));
        JBPopupFactory.getInstance().createActionGroupPopup(CHOOSER_TITLE, group, context, mnemonics, true).showInBestPositionFor(editor);
    }

    static void generateHandlingSequences(Project myProject, Editor myEditor, XHandGenerator myGenerator) {
        Document document = myEditor.getDocument();
        document.insertString(myEditor.getCaretModel().getOffset(), myGenerator.getTemplateDescription());
    }

    private static class XHandGeneratorAction extends AnAction {
        public XHandGenerator getMyGenerator() {
            return myGenerator;
        }

        private final XHandGenerator myGenerator;
        private final Project myProject;
        private final Editor myEditor;

        public XHandGeneratorAction(XHandGenerator generator, Project project, Editor editor, char mnemonic) {
            super(UIUtil.MNEMONIC + String.valueOf(mnemonic) + ". " + generator.getTemplateDescription());
            myGenerator = generator;
            myProject = project;
            myEditor = editor;
        }

        @Override
        public void actionPerformed(AnActionEvent e) {
            if (!FileDocumentManager.getInstance().requestWriting(myEditor.getDocument(), myProject)) {
                return;
            }

            WriteCommandAction.runWriteCommandAction(myProject, () -> generateHandlingSequences(myProject, myEditor, myGenerator));
        }
    }

    class AdditionalEventInfo {
        @Nullable
        private Editor myEditor;
        @Nullable private Module myModule;
        @Nullable private PsiElement myElement;

        @Nullable
        Module getModule() {
            return myModule;
        }

        AdditionalEventInfo setModule(@Nullable Module module) {
            myModule = module;
            return this;
        }

        @Nullable
        Editor getEditor() {
            return myEditor;
        }

        @Nullable
        PsiElement getElement() {
            return myElement;
        }

        AdditionalEventInfo setPsiElement(@Nullable PsiElement element) {
            myElement = element;
            return this;
        }

        AdditionalEventInfo setEditor(@Nullable Editor editor) {
            myEditor = editor;
            return this;
        }
    }
}
