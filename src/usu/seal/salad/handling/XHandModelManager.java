package usu.seal.salad.handling;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.opencsv.CSVReader;
import usu.seal.salad.cfg.node.SMethodSignature;
import usu.seal.salad.exception.ExceptionExample;
import usu.seal.salad.util.Constants;
import usu.seal.salad.util.MapUtil;
import usu.seal.salad.util.TypeUtil;

import java.io.*;
import java.util.*;

public class XHandModelManager {
    private BiMap<String, Integer> objectBiMap;
    private BiMap<String, Integer> methodBiMap;

    public Map<String, String> getHandlingMethodMap() {
        return handlingMethodMap;
    }

    private Map<String, String> handlingMethodMap;
    private Map<Integer, List<HandlingInstance>> handlingMap;

    private ArrayList<ExceptionExample> exceptionExamples;

    public XHandModelManager() throws IOException{
        objectBiMap = HashBiMap.create();
        methodBiMap = HashBiMap.create();
        handlingMethodMap = new HashMap<>();
        handlingMap = new HashMap<>();
        loadData();
        loadExceptionExamples();
        buildFreqModel();
    }

    private void buildFreqModel() throws IOException {
        for (Map.Entry<Integer, List<HandlingInstance>> entry : handlingMap.entrySet()) {
            String objectType = objectBiMap.inverse().get(entry.getKey());
            Map<Integer, Integer> methodFreq = new HashMap<>();
            for (HandlingInstance handlingInstance : entry.getValue()) {
                Set<Integer> set = new HashSet<>();
                for (Integer methodId : handlingInstance.getHandlingSequence()) {
                    if (!set.contains(methodId)) set.add(methodId);
                }
                for (Integer methodId : set) {
                    Integer count = methodFreq.get(methodId);
                    if (count == null) methodFreq.put(methodId, 1);
                    else methodFreq.put(methodId, count+1);
                }
            }
            List<Map.Entry<Integer, Integer>> entries = MapUtil.entriesSortedByValues(methodFreq);
            for (Map.Entry<Integer, Integer> freqEntry : entries) {
                String methodString = methodBiMap.inverse().get(freqEntry.getKey());
                if (methodString.startsWith(objectType)) {
                    SMethodSignature sMethodSignature = TypeUtil.convertMethodDalvikString(methodString);
                    if (sMethodSignature.getParameters().length == 0) {
                        handlingMethodMap.put(objectType, methodString);
                        break;
                    }
                }
            }
        }

        //remove few general objects
        InputStream resourceAsStream = this.getClass().getResourceAsStream(Constants.XHAND_COMMONTYPES_FILE);
        CSVReader objectsCsvReader = new CSVReader(new InputStreamReader(resourceAsStream));
        String[] readEntry;
        while ((readEntry = objectsCsvReader.readNext()) != null) {
            handlingMethodMap.remove(readEntry[0]);
        }
        objectsCsvReader.close();
    }

    private void loadData() throws IOException  {
            InputStream resourceAsStream = this.getClass().getResourceAsStream(Constants.XHAND_OBJECTS_FILE);
            CSVReader objectsCsvReader = new CSVReader(new InputStreamReader(resourceAsStream));
            String[] readEntry;
            while ((readEntry = objectsCsvReader.readNext()) != null) {
                objectBiMap.put(readEntry[0], Integer.parseInt(readEntry[1]));
            }
            objectsCsvReader.close();

            resourceAsStream = this.getClass().getResourceAsStream(Constants.XHAND_METHODS_FILE);
            CSVReader methodsCsvReader = new CSVReader(new InputStreamReader(resourceAsStream));
            while ((readEntry = methodsCsvReader.readNext()) != null) {
                methodBiMap.put(readEntry[0], Integer.parseInt(readEntry[1]));
            }
            methodsCsvReader.close();

            resourceAsStream = this.getClass().getResourceAsStream(Constants.XHAND_HANDLING_FILE);
            CSVReader handlingCSVReader = new CSVReader(new InputStreamReader(resourceAsStream));
            while ((readEntry = handlingCSVReader.readNext()) != null) {
                Integer objectId = Integer.valueOf(readEntry[0]);
                List<Integer> featureSet = readStringList(readEntry[1]);
                Integer exceptionId = Integer.valueOf(readEntry[2]);
                List<Integer> handlingSequence = readStringList(readEntry[3]);
                HandlingInstance handlingInstance = new HandlingInstance(featureSet, exceptionId, handlingSequence);
                if (!handlingMap.containsKey(objectId)) {
                    handlingMap.put(objectId, new ArrayList<>());
                }
                handlingMap.get(objectId).add(handlingInstance);
            }
            handlingCSVReader.close();
    }

    private List<Integer> readStringList(String str) {
        List<Integer> result = new ArrayList<>();
        String replace = str.replace("[", "");
        replace = replace.replace("]", "");
        if (!replace.contains(", ")) {
            result.add(Integer.valueOf(replace));
            return result;
        } else {
            List<String> myList = new ArrayList<String>(Arrays.asList(replace.split(", ")));
            for (String numString : myList) {
                result.add(Integer.valueOf(numString));
            }
            return result;
        }
    }



    private void loadExceptionExamples() {
        try {
            InputStream resourceAsStream = this.getClass().getResourceAsStream(Constants.XHAND_EXAMPLE_FILE);
            ObjectInputStream  ois = new ObjectInputStream(resourceAsStream);
            exceptionExamples = (ArrayList<ExceptionExample>) ois.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<ExceptionExample> getExceptionExamples() {
        return exceptionExamples;
    }
}
