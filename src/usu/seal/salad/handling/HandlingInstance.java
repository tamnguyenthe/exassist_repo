package usu.seal.salad.handling;

import java.util.List;

public class HandlingInstance {
    private List<Integer> featureSet;
    private Integer exceptionId;
    private List<Integer> handlingSequence;

    public HandlingInstance(List<Integer> featureSet, Integer exceptionId, List<Integer> handlingSequence) {
        this.featureSet = featureSet;
        this.exceptionId = exceptionId;
        this.handlingSequence = handlingSequence;
    }


    public List<Integer> getFeatureSet() {
        return featureSet;
    }

    public void setFeatureSet(List<Integer> featureSet) {
        this.featureSet = featureSet;
    }

    public Integer getExceptionId() {
        return exceptionId;
    }

    public void setExceptionId(Integer exceptionId) {
        this.exceptionId = exceptionId;
    }

    public List<Integer> getHandlingSequence() {
        return handlingSequence;
    }

    public void setHandlingSequence(List<Integer> handlingSequence) {
        this.handlingSequence = handlingSequence;
    }
}
