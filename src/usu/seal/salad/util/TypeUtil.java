package usu.seal.salad.util;

import usu.seal.salad.cfg.node.SMethodSignature;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TamNT on 7/8/15.
 */
public class TypeUtil {
    //convert qualified name to Dalvik name
    public static String convertClassStringToDalvik(String standard) {
        int c = 0;
        if (standard.endsWith("...")) {
            standard = standard.substring(0, standard.length() - 3);
            c++;
        }
        int l = standard.length();
        while (l >= 2 && standard.substring(l-2).equals("[]")) {
            standard = standard.substring(0, standard.length()-2);
            l = standard.length();
            c++;
        }
        int left = 0;
        while (left < standard.length() && standard.charAt(left) != '<') left++;
        if (left < standard.length()) standard = standard.substring(0, left);
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < c; i++) stringBuilder.append("[");
        if (PrimitiveType.getPrimitiveMap().containsKey(standard)) {
            stringBuilder.append(PrimitiveType.getPrimitiveMap().get(standard));
        } else if (standard.length() == 1) {
            stringBuilder.append("Ljava/lang/Object;");
        } else {
            stringBuilder.append("L");
            String[] parts = standard.split("\\.");
            if (parts.length >= 2 && Character.isUpperCase(parts[parts.length-2].charAt(0))) {
                stringBuilder.append(parts[0]);
                for (int i = 1; i < parts.length-1; i++) {
                    stringBuilder.append("/" + parts[i]);
                }
                stringBuilder.append("$" + parts[parts.length-1]);
            } else {
                stringBuilder.append(standard.replace('.', '/'));
            }
            stringBuilder.append(";");
        }
        return stringBuilder.toString();
    }

    //convert Dalvik name to qualified name
    public static String convertDalvikStringToClass(String dalvikString) {
        int i = 0;
        while (dalvikString.charAt(0) == '[') {
            i++;
            dalvikString = dalvikString.substring(1);
        }
        StringBuilder stringBuilder = new StringBuilder();
        if (!isObjectType(dalvikString)) stringBuilder.append(PrimitiveType.getPrimitiveMap().inverse().get(dalvikString));
        else stringBuilder.append(dalvikString.substring(1, dalvikString.length() - 1).replace('/', '.'));
        for (int j = 0; j < i; j++) {
            stringBuilder.append("[]");
        }
        return stringBuilder.toString();
    }

    public static boolean isObjectType(String str) { return str.charAt(0) == 'L'; }
    public static boolean isArrayType(String str) { return  str.charAt(0) == '['; }
    //???
    public static boolean isMethod(String dalvikString) { return  dalvikString.charAt(0) == 'L'; }
    public static boolean isJavaClass(String type) {
        return type.indexOf(".") > 0;
    }
    public static String getClassNameFromQualifiedName(String type) {
        String[] parts = type.split("\\.|\\$");
        return parts[parts.length-1];
    }

    //convert Dalvik method string to SMethodSignature
    public static SMethodSignature convertMethodDalvikString(String dalvikString) {
        if (!isMethod(dalvikString)) return null;
        SMethodSignature methodSignature = new SMethodSignature();
        String[] parts = dalvikString.split("(->)|\\(|\\)");
        //containing class
        methodSignature.setContainingClass(convertDalvikStringToClass(parts[0]));
        //name
        methodSignature.setName(parts[1]);

        //parameters
        String parameterListString = parts[2];
        List<String> parameterList = getParameterList(parameterListString);
        String[] parameters = new String[parameterList.size()];
        for (int i = 0; i < parameterList.size(); i++) {
            parameters[i] = parameterList.get(i);
        }
        methodSignature.setParameters(parameters);
        //return type
        if (isObjectType(parts[3])) methodSignature.setReturnType(convertDalvikStringToClass(parts[3]));
        else if (isArrayType(parts[3])) methodSignature.setReturnType(convertDalvikStringToClass(parts[3]));
        else methodSignature.setReturnType(PrimitiveType.getPrimitiveMap().inverse().get(parts[3]));

        if (methodSignature.getName().equals(Constants.INIT)) {
            methodSignature.setName(methodSignature.getContainingClass());
            methodSignature.setReturnType(methodSignature.getContainingClass());
        }
        return methodSignature;
    }

    //get parameter list from parameter string
    public static List<String> getParameterList(String parameterListString) {
        if (parameterListString.length() == 0) return new ArrayList<>();
        else {
            List<String> result = new ArrayList<>();
            if (isObjectType(parameterListString)) {
                int endOfTypeString = parameterListString.indexOf(';');
                result.add(convertDalvikStringToClass(parameterListString.substring(0,endOfTypeString+1)));
                String newString = parameterListString.substring(endOfTypeString+1);
                result.addAll(getParameterList(newString));
            } else if (isArrayType(parameterListString)) {
                String newString = parameterListString.substring(1);
                List<String> parameterList = getParameterList(newString);
                parameterList.set(0, parameterList.get(0).concat("[]"));
                result.addAll(parameterList);
            } else {
                result.add(PrimitiveType.getPrimitiveMap().inverse().get(Character.toString(parameterListString.charAt(0))));
                String newString = parameterListString.substring(1);
                result.addAll(getParameterList(newString));
            }
            return result;
        }
    }

}
