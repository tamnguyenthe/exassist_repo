package usu.seal.salad.util;


/**
 * Created by TamNT on 7/7/15.
 */
public class Constants {
    public static final String INIT = "<init>";
    public static final String ANDROID_API_FILE = "/android_packages.csv";

    public static final String LOAD = "<LOAD>";
    public static final int LOAD_ID = -1;
    public static final char COMMA_CHAR = ',';
    public static final char QUOTE_CHAR = '\"';

    public static final String OBJECTS_FILE = "/xrank/Objects.csv";
    public static final String METHOD_NAMES_FILE = "/xrank/Methods.csv";
    public static final String N_M = "/xrank/MethodCounts.csv";
    public static final String N_E = "/xrank/ExceptionCounts.csv";
    public static final String N_ME = "/xrank/MethodExceptions.csv"; //use later
    public static final double EPSILON = 0.00000000001;

    public static final String XHAND_OBJECTS_FILE = "/xhand/XHandObjectBiMap.csv";
    public static final String XHAND_METHODS_FILE = "/xhand/XHandMethodBiMap.csv";
    public static final String XHAND_HANDLING_FILE = "/xhand/XHandHandlingMap.csv";
    public static final String XHAND_COMMONTYPES_FILE = "/xhand/CommonObjectTypes.csv";
    public static final String XHAND_EXAMPLE_FILE = "/xhand/ExceptionExamples1.ser";
}
