package usu.seal.salad.util;

import com.opencsv.CSVReader;
import usu.seal.salad.cfg.node.SMethodNode;
import usu.seal.salad.cfg.node.SMethodSignature;
import usu.seal.salad.cfg.node.SObjectNode;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by TamNT on 8/18/2015.
 */
public class AndroidAPIUtils {
    private static List<String> androidPackages = null;

    public static List<String> getAndroidPackages() {
        if (androidPackages == null) {
            androidPackages = new ArrayList<>();
            try {
                InputStream packageAsStream = AndroidAPIUtils.class.getResourceAsStream(Constants.ANDROID_API_FILE);
                CSVReader packageReader = new CSVReader(new InputStreamReader(packageAsStream));
                String nextLine[];
                while ((nextLine = packageReader.readNext()) != null) {
                    androidPackages.add(nextLine[0]);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return androidPackages;
    }

    public static boolean isAndroidAPI(SMethodNode methodNode) {
        SMethodSignature methodSignature = methodNode.getSignature();
        for (String androidPackage : getAndroidPackages()) {
            if (methodSignature.getContainingClass().startsWith(androidPackage)) return true;
        }
        return false;
    }

    public static boolean isAndroidAPI(SObjectNode objectNode) {
        for (String androidPackage : getAndroidPackages()) {
            if (objectNode.getType().startsWith(androidPackage)) return true;
        }
        return false;
    }

    // simple test for loading android packages
    public static void main(String[] args) {
        System.out.println("Number of packages: " + getAndroidPackages().size());
        System.out.println(getAndroidPackages());
    }

}
