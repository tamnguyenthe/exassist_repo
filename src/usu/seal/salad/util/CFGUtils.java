package usu.seal.salad.util;

import com.google.common.collect.ImmutableList;
import usu.seal.salad.cfg.node.SNode;
import usu.seal.salad.cfg.node.SObjectNode;

import java.util.Set;

/**
 * Created by TamNT on 8/18/2015.
 */
public class CFGUtils {

    public static boolean isMethodOrControlNode(SNode node) {
        return (node instanceof SObjectNode) ? false : true;
    }

    public static boolean hasIntersection(ImmutableList<Integer> idList, Set<Integer> newIdSet) {
        for (Integer id : idList) {
            if (newIdSet.contains(id)) {
                return true;
            }
        }
        return false;
    }
}
