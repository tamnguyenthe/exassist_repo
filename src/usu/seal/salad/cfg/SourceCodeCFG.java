package usu.seal.salad.cfg;

import usu.seal.salad.cfg.node.SNode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tam Nguyen on 6/11/2015.
 */

public class SourceCodeCFG {
    private List<SNode> nodes;
    private SNode in; // the start node
    private List<SNode> outs; // exist nodes

    public SourceCodeCFG() {
        in = null;
        this.nodes = new ArrayList<>();
        this.outs = new ArrayList<>();
    }

    // add a new node to current GROUM
    public void add(SNode node) {
        nodes.add(node);
    }

    // merge with another GROUM
    public void merge(SourceCodeCFG subGraph) {
        for (SNode node: subGraph.getNodes())
            add(node);
        if (subGraph.getIn() != null) {
            for (SNode outNode : getOuts())
                outNode.getControlEdges().add(subGraph.getIn());
            getOuts().clear();
            for (SNode outNodeSubGraph : subGraph.getOuts())
                getOuts().add(outNodeSubGraph);
            if (getIn() == null) setIn(subGraph.getIn());
        }
    }

    // merge a method/control node to GROUM (this will update in and outs of current GROUM)
    public void merge(SNode newNode) {
        add(newNode);
        for (SNode outNode: getOuts())
            outNode.getControlEdges().add(newNode);
        getOuts().clear();
        getOuts().add(newNode);
        if(getIn() == null) setIn(newNode);
    }

    // update backward data edges
    public void addDataBackEdges() {
        for (SNode node: nodes) {
            for (SNode outNode: node.getDataEdges())
                if (outNode != null)
                    outNode.getDataBackEdges().add(node);
        }
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (SNode node : nodes) {
            stringBuilder.append(node.toString());
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }

    public List<SNode> getNodes() {
        return nodes;
    }

    public void setNodes(List<SNode> nodes) {
        this.nodes = nodes;
    }

    public SNode getIn() {
        return in;
    }

    public void setIn(SNode in) {
        this.in = in;
    }

    public List<SNode> getOuts() {
        return outs;
    }

    public void setOuts(List<SNode> outs) {
        this.outs = outs;
    }

}
