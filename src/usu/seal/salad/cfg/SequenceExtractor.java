package usu.seal.salad.cfg;

import com.google.common.collect.ImmutableList;
import com.intellij.openapi.components.ServiceManager;
//import usu.seal.salad.analyzing.MemoryManager;
import usu.seal.salad.cfg.node.SControlNode;
import usu.seal.salad.cfg.node.SMethodNode;
import usu.seal.salad.cfg.node.SNode;
import usu.seal.salad.cfg.node.SObjectNode;
import usu.seal.salad.util.AndroidAPIUtils;
import usu.seal.salad.util.CFGUtils;
import usu.seal.salad.util.TypeUtil;

import java.util.*;

/**
 * Created by TamNT on 8/18/2015.
 */
public class SequenceExtractor {
    private SourceCodeCFG cfg;

    private Map<Integer, Set<ImmutableList<Integer>>> singleObjectUsages;
    private Map<ImmutableList<Integer>, Set<ImmutableList<Integer>>> objectSetUsages;

    public SequenceExtractor(SourceCodeCFG cfg) {
        this.cfg = cfg;
        singleObjectUsages = new HashMap<>();
        objectSetUsages = new HashMap<>();
    }

    public void collectUsages() {
        travelCFG();
        Map<Integer, SNode> nodeMap = new HashMap<>();
        for (SNode sNode : cfg.getNodes()) nodeMap.put(sNode.getId(), sNode);

//        MemoryManager memoryManager = ServiceManager.getService(MemoryManager.class);


        for (Map.Entry<Integer, Set<ImmutableList<Integer>>> entry : singleObjectUsages.entrySet()) {
            SObjectNode objectNode = (SObjectNode) nodeMap.get(entry.getKey());
            String objectDalvikString = TypeUtil.convertClassStringToDalvik(objectNode.getType());

            for (ImmutableList<Integer> sequence : entry.getValue()) {
                if (sequence.size() >= 2 && sequence.size() < 20) {
                    List<String> methodSequence = new ArrayList<>();
                    for (Integer methodNodeID : sequence) {
                        SMethodNode methodNode = (SMethodNode)nodeMap.get(methodNodeID);
                        methodSequence.add(methodNode.getSignature().toDalvikString());
                    }
                    System.out.println(methodSequence);
//                    memoryManager.put(objectDalvikString, methodSequence);
                }
            }
        }

        for (Map.Entry<ImmutableList<Integer>, Set<ImmutableList<Integer>>> entry : objectSetUsages.entrySet()) {
            List<String> typeList = new ArrayList<>();
            for (Integer objectNodeID : entry.getKey()) {
                SObjectNode objectNode = (SObjectNode) nodeMap.get(objectNodeID);
                typeList.add(TypeUtil.convertClassStringToDalvik(objectNode.getType()));
            }
            Collections.sort(typeList);

            for (ImmutableList<Integer> sequence : entry.getValue()) {
                if (sequence.size() >= 2 && sequence.size() < 20) {
                    List<String> methodSequence = new ArrayList<>();
                    for (Integer methodNodeID : sequence) {
                        SMethodNode methodNode = (SMethodNode)nodeMap.get(methodNodeID);
                        methodSequence.add(methodNode.getSignature().toDalvikString());
                    }
//                    memoryManager.put(typeList, methodSequence);
                }
            }
        }
    }

    // don't analyze cfg it has to many if/for node
    private boolean checkCfg() {
        int nControlNodes = 0;
        for (SNode sNode : cfg.getNodes()) {
            if (sNode instanceof SControlNode) {
                SControlNode controlNode = (SControlNode) sNode;
                if (!controlNode.getLabel().equals(SControlNode.RETURN_LABEL)) {
                    nControlNodes++;
                }
            }
        }
        return nControlNodes <= 14;
    }

    private void travelCFG() {
        if (!checkCfg()) return;

        // find start node
        SNode startNode = null;
        for (SNode node : cfg.getNodes()) {
            if (CFGUtils.isMethodOrControlNode(node)) {
                startNode = node;
                break;
            }
        }
        if (startNode == null) return;

        // initialize frontier
        CFGState startState = new CFGState(startNode);
        Stack<CFGState> frontier = new Stack<>();
        frontier.push(startState);

        // main loop
        while (!frontier.isEmpty()) {
            CFGState currentState = frontier.pop();
            SNode currentNode = currentState.getStartNode();
            currentState.getExploredPaths().add(currentNode);
            while (currentNode.getControlEdges().size() == 1) {
                currentState.getVisitedNodeList().add(currentNode);
                currentNode = (SNode)currentNode.getControlEdges().toArray()[0];
            }
            currentState.getVisitedNodeList().add(currentNode);
            if (currentNode.getControlEdges().size() == 0) {
                collectUsagesOnPath(currentState.getVisitedNodeList());
            } else {
                for (SNode nextNode : currentNode.getControlEdges()) {
                    if (!currentState.getExploredPaths().contains(nextNode)) {
                        CFGState nextState = new CFGState(nextNode, currentState.getVisitedNodeList(), currentState.getExploredPaths());
                        frontier.push(nextState);
                    }
                }
            }
        }
    }

    private void collectUsagesOnPath(List<SNode> visitedNodeList) {
        Map<Integer, List<Integer>> singleUsages = new HashMap<>();
        Map<ImmutableList<Integer>, List<Integer>> setUsages = new HashMap<>();
        Set<Integer> idSet;
        Set<String> typeSet;
        for (SNode node : visitedNodeList) {
            if (node instanceof SMethodNode) {
                SMethodNode methodNode = (SMethodNode)node;
                if (AndroidAPIUtils.isAndroidAPI(methodNode)) {
                    idSet = new TreeSet<>();
                    typeSet = new HashSet<>();
                    Set<SNode> relatedDataNodes = methodNode.getRelatedDataNodes();
                    for (SNode sNode : relatedDataNodes) {
                        SObjectNode objectNode = (SObjectNode)sNode;
                        if (AndroidAPIUtils.isAndroidAPI(objectNode)) {
                            // single usages
                            if (!singleUsages.containsKey(objectNode.getId())) singleUsages.put(objectNode.getId(), new ArrayList<Integer>());
                            singleUsages.get(objectNode.getId()).add(methodNode.getId());
                            // form a id set
                            if (!typeSet.contains(objectNode.getType())) {
                                typeSet.add(objectNode.getType());
                                idSet.add(objectNode.getId());
                            }
                        }
                    }
                    // update previous set usages
                    for (Map.Entry<ImmutableList<Integer>, List<Integer>> entry : setUsages.entrySet()) {
                        if (CFGUtils.hasIntersection(entry.getKey(), idSet)) entry.getValue().add(methodNode.getId());
                    }
                    // add new set usage if have
                    if (idSet.size() >= 2) {
                        ImmutableList<Integer> idList = ImmutableList.copyOf(idSet);
                        if (!setUsages.containsKey(idList)) {
                            setUsages.put(idList, new ArrayList<Integer>());
                            setUsages.get(idList).add(methodNode.getId());
                        }
                    }
                }
            }
        }
        // collect single usages
        for (Map.Entry<Integer, List<Integer>> entry : singleUsages.entrySet()) {
            if (!singleObjectUsages.containsKey(entry.getKey())) singleObjectUsages.put(entry.getKey(), new HashSet<ImmutableList<Integer>>());
            singleObjectUsages.get(entry.getKey()).add(ImmutableList.copyOf(entry.getValue()));
        }
        // collect set usages
        for (Map.Entry<ImmutableList<Integer>, List<Integer>> entry : setUsages.entrySet()) {
            if (!objectSetUsages.containsKey(entry.getKey())) objectSetUsages.put(entry.getKey(), new HashSet<ImmutableList<Integer>>());
            objectSetUsages.get(entry.getKey()).add(ImmutableList.copyOf(entry.getValue()));
        }
    }

        /*
    public CompletionQuery createCompletionQuery() {
        CompletionQuery completionQuery = new CompletionQuery(cfgParameters.getEditingObjectNode().getType());
        Set<SNode> relatedMethodNodes = cfgParameters.getEditingObjectNode().getRelatedDataNodes();
        Set<Set<String>> multipleObjectsSet = new HashSet<>();
        for (SNode relatedMethodNode : relatedMethodNodes) {
            SMethodNode methodNode = (SMethodNode)relatedMethodNode;
            Set<SNode> objectSet = methodNode.getRelatedDataNodes();
            Set<String> multipleObject = new HashSet<>();
            for (SNode node : objectSet) {
                SObjectNode objectNode = (SObjectNode)node;
                multipleObject.add(objectNode.getType());
            }
            if (multipleObject.size() > 1) {
                multipleObjectsSet.add(multipleObject);
            }
        }
        completionQuery.setMultipleObjectsSet(multipleObjectsSet);

        //find start node
        SNode startNode = null;
        for (SNode node : cfgParameters.getCfg().getNodes()) {
            if (!(node instanceof SObjectNode)) {
                startNode = node;
                break;
            }
        }
        CFGState startState = new CFGState(startNode);
        Stack<CFGState> frontier = new Stack<>();
        frontier.add(startState);

        while (!frontier.isEmpty()) {
            CFGState currentState = frontier.pop();
            currentState.getExploredPaths().add(currentState.getStartNode());
            SNode currentNode = currentState.getStartNode();
            while (currentNode.getControlEdges().size() == 1) {
                currentState.getVisitedNodeList().add(currentNode);
                currentNode = (SNode)currentNode.getControlEdges().toArray()[0];
            }
            currentState.getVisitedNodeList().add(currentNode);
            if (currentNode.getControlEdges().size() == 0) {
                List<SMethodSignature> methodSequence = new ArrayList<>();
                List<Integer> positions = new ArrayList<>();
                boolean isCompletePath = true;
                for (SNode node : currentState.getVisitedNodeList()) {
                    System.out.println(node);
                    if (relatedMethodNodes.contains(node)) {
                        SMethodNode methodNode = (SMethodNode)node;
                        methodSequence.add(methodNode.getSignature());
                        if (methodNode.getSignature().equals(SMethodSignature.DUMMY_METHOD_SIGNATURE)) {
                            positions.add(methodSequence.size() - 1);
                            isCompletePath = false;
                        }
                    }
                }
                if (!isCompletePath) {
                    completionQuery.addMethodSequence(Pair.of(methodSequence, positions));
                }
            } else {
                for (SNode nextNode : currentNode.getControlEdges()) {
                    if (!currentState.getExploredPaths().contains(nextNode)) {
                        CFGState nextState = new CFGState(nextNode, currentState.getVisitedNodeList(), currentState.getExploredPaths());
                        frontier.push(nextState);
                    }
                }
            }
        }
        return completionQuery;
    }

    public List<SMethodSignature> getMethodSequenceForEvaluation() {
        List<SMethodSignature> result = new ArrayList<>();
        Set<SNode> relatedMethodNodes = cfgParameters.getEvalObjectNode().getRelatedDataNodes();
        for (SNode relatedMethodNode : relatedMethodNodes) {
            SMethodNode methodNode = (SMethodNode)relatedMethodNode;
            result.add(methodNode.getSignature());
        }
        return result;
    }

    public CompletionParameters getCompletionParameters() {
        return completionParameters;
    }

    public void setCompletionParameters(CompletionParameters completionParameters) {
        this.completionParameters = completionParameters;
    }


    private void collectAPIObjects() {
        Set<Integer> apiObjectIds = new HashSet<>();
        Set<ImmutableList<Integer>> apiObjectSetIDs = new HashSet<>();
        Set<Integer> idSet;
        Set<String> typeSet;
        for (SNode node : cfg.getNodes()) {
            if (node instanceof SMethodNode) {
                SMethodNode methodNode = (SMethodNode)node;
                // collect object nodes
                if (AndroidAPIUtils.isAndroidAPI(methodNode)) {
                    idSet = new TreeSet<>();
                    typeSet = new HashSet<>();
                    for (SNode sNode : methodNode.getRelatedDataNodes()) {
                        SObjectNode objectNode = (SObjectNode)sNode;
                        if (AndroidAPIUtils.isAndroidAPI(objectNode)) {
                            apiObjectIds.add(objectNode.getId());
                            if (!typeSet.contains(objectNode.getType())) {
                                typeSet.add(objectNode.getType());
                                idSet.add(objectNode.getId());
                            }
                        }
                    }
                    if (idSet.size() >= 2) apiObjectSetIDs.add(ImmutableList.copyOf(idSet));
                }
            }
        }
        for (Integer apiObjectID : apiObjectIds) singleObjectUsages.put(apiObjectID, new HashSet<ImmutableList<Integer>>());
        for (ImmutableList<Integer> apiObjectIdSet : apiObjectSetIDs) objectSetUsages.put(apiObjectIdSet, new HashSet<ImmutableList<Integer>>());
    }
    */
}
