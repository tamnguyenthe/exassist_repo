package usu.seal.salad.cfg;

/**
 * Created by Tam Nguyen on 6/22/15.
 */
public class CFGParameters {
    private SourceCodeCFG cfg; // final GROUM
    private int numberOfNodes; // number of nodes, used to index nodes
    private IdentStack identStack; // stack to store variable mapping

    public CFGParameters(){
        cfg = new SourceCodeCFG();
        numberOfNodes = 0;
        identStack = new IdentStack();
    }

    public int idForNewNode() {
        return numberOfNodes++;
    }

    public int getNumberOfNodes() {
        return numberOfNodes;
    }

    public void setNumberOfNodes(int numberOfNodes) {
        this.numberOfNodes = numberOfNodes;
    }

    public IdentStack getIdentStack() {
        return identStack;
    }

    public void setIdentStack(IdentStack identStack) {
        this.identStack = identStack;
    }

    public SourceCodeCFG getCfg() {
        return cfg;
    }

    public void setCfg(SourceCodeCFG cfg) {
        this.cfg = cfg;
    }

}
