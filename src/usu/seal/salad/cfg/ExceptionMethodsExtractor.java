package usu.seal.salad.cfg;


import com.intellij.openapi.components.ServiceManager;
//import usu.seal.salad.analyzing.ExceptionMemoryManager;
import usu.seal.salad.cfg.node.SMethodNode;
import usu.seal.salad.cfg.node.SNode;
import usu.seal.salad.util.TypeUtil;

import java.util.*;

/**
 * Created by Tam Nguyen on 2/1/2016.
 */
public class ExceptionMethodsExtractor {
    private CFGParameters cfgParameters;
    private SourceCodeCFG sourceCodeCFG;

    public Map<String, List<String>> getObjectMethodMap() {
        return objectMethodMap;
    }

    private Map<String, List<String>> objectMethodMap;

    public ExceptionMethodsExtractor(CFGParameters cfgParameters, SourceCodeCFG sourceCodeCFG) {
        this.cfgParameters = cfgParameters;
        this.sourceCodeCFG = sourceCodeCFG;
        objectMethodMap = new HashMap<>();
    }

    public void extract() {
        if (sourceCodeCFG.getIn() == null) return;
        SNode in = sourceCodeCFG.getIn();
        Stack<SNode> frontier = new Stack<>();
        frontier.push(in);
        Set<Integer> visitedNodes = new HashSet<>();
        SNode currentNode = null;
        while (!frontier.isEmpty()) {
            currentNode = frontier.pop();

            if (currentNode instanceof SMethodNode) {
                SMethodNode methodNode = (SMethodNode) currentNode;
                String objectType = TypeUtil.convertClassStringToDalvik(methodNode.getSignature().getContainingClass());
                if (!objectMethodMap.containsKey(objectType)) objectMethodMap.put(objectType, new ArrayList<>());
                objectMethodMap.get(objectType).add(methodNode.getSignature().toDalvikString());
            }

            if (!visitedNodes.contains(currentNode.getId())) {
                visitedNodes.add(currentNode.getId());
                for (SNode nextNode : currentNode.getControlEdges()) {
                    frontier.push(nextNode);
                }
            }
        }
    }

    /*
    public void collect() {
        ExceptionMemoryManager manager = ServiceManager.getService(ExceptionMemoryManager.class);
        for (Map.Entry<String, List<String>> entry : getObjectMethodMap().entrySet()) {
            manager.addMethods(entry.getKey(), entry.getValue());
        }
    }
    */

}
