package usu.seal.salad.cfg.visitor;

import com.intellij.psi.JavaElementVisitor;
import com.intellij.psi.PsiCodeBlock;
import com.intellij.psi.PsiStatement;
import usu.seal.salad.cfg.CFGParameters;
import usu.seal.salad.cfg.SourceCodeCFG;

/**
 * Created by Tam Nguyen on 6/18/2015.
 */
public class SourceCodeCFGVisitor extends JavaElementVisitor {

    protected CFGParameters cfgParameters;

    protected SourceCodeCFG subGraph;

    public SourceCodeCFGVisitor(CFGParameters cfgParameters) {
        this.subGraph = new SourceCodeCFG();
        this.cfgParameters = cfgParameters;
    }

    @Override
    public void visitCodeBlock(PsiCodeBlock block) {
        cfgParameters.getIdentStack().pushANewMap();
        for (PsiStatement psiStatement : block.getStatements()) {
            if (psiStatement != null) {
                CFGStatementVisitor statementVisitor = new CFGStatementVisitor(cfgParameters);
                psiStatement.accept(statementVisitor);
                subGraph.merge(statementVisitor.getSubGraph());
            }
        }
        cfgParameters.getIdentStack().pop();
    }

    public SourceCodeCFG getSubGraph() {
        return subGraph;
    }

    public void setSubGraph(SourceCodeCFG subGraph) {
        this.subGraph = subGraph;
    }

    public CFGParameters getCfgParameters() {
        return cfgParameters;
    }

    public void setCfgParameters(CFGParameters cfgParameters) {
        this.cfgParameters = cfgParameters;
    }

}