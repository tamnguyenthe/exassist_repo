package usu.seal.salad.cfg.visitor;

import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.markup.HighlighterLayer;
import com.intellij.openapi.editor.markup.HighlighterTargetArea;
import com.intellij.openapi.editor.markup.TextAttributes;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.JavaElementVisitor;
import com.intellij.psi.PsiMethodCallExpression;
import usu.seal.salad.exception.XRankModelManager;

import javax.swing.text.Highlighter;
import java.awt.*;
import java.util.Random;

public class HighlightExpressionVisitor extends JavaElementVisitor {

    private double risk;
    private Editor editor;

    public HighlightExpressionVisitor(Editor editor) {
        this.editor = editor;
    }

    public HighlightExpressionVisitor(Editor editor, double risk) {
        this.editor = editor;
        this.risk = risk;
    }

    @Override
    public void visitMethodCallExpression(PsiMethodCallExpression expression) {
        TextRange textRange = expression.getTextRange();
        TextAttributes attributes = new TextAttributes();
        System.out.println(risk);
        if (risk > 0.1) {
            attributes.setBackgroundColor(Color.red);
        } else if (risk < 0.1 && risk > 0.05) {
            attributes.setBackgroundColor(Color.orange);
        } else if (risk < 0.05 && risk > 0.001) {
            attributes.setBackgroundColor(Color.yellow);
        }
        System.out.println(textRange);
        editor.getMarkupModel().addRangeHighlighter(textRange.getStartOffset(), textRange.getEndOffset(), HighlighterLayer.SELECTION - 100, attributes, HighlighterTargetArea.EXACT_RANGE);
    }
}
