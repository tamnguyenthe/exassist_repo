package usu.seal.salad.cfg.visitor;

import com.intellij.psi.*;
import usu.seal.salad.cfg.CFGParameters;
import usu.seal.salad.cfg.SourceCodeCFG;
import usu.seal.salad.cfg.node.SControlNode;
import usu.seal.salad.cfg.node.SMethodNode;
import usu.seal.salad.cfg.node.SMethodSignature;
import usu.seal.salad.cfg.node.SObjectNode;
import usu.seal.salad.util.PsiUtils;

/**
 * Created by Student on 6/18/2015.
 */
public class CFGExpressionVisitor extends SourceCodeCFGVisitor {
    private SObjectNode outObjectNode;
    public CFGExpressionVisitor(CFGParameters cfgParameters) {
        super(cfgParameters);
    }

    // OK
    @Override
    public void visitPolyadicExpression(PsiPolyadicExpression expression) {
        PsiExpression[] operandExpressions = expression.getOperands();
        for (PsiExpression operandExpression : operandExpressions) {
            if (operandExpression != null) {
                CFGExpressionVisitor operandExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
                operandExpression.accept(operandExpressionVisitor);
                subGraph.merge(operandExpressionVisitor.getSubGraph());
            }
        }
    }

    // OK
    @Override
    public void visitBinaryExpression(PsiBinaryExpression expression) {
        PsiExpression lExpression = expression.getLOperand();
        CFGExpressionVisitor lExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
        lExpression.accept(lExpressionVisitor);
        subGraph.merge(lExpressionVisitor.getSubGraph());

        PsiExpression rExpression = expression.getROperand();
        if (rExpression != null) {
            CFGExpressionVisitor rExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
            rExpression.accept(rExpressionVisitor);
            subGraph.merge(rExpressionVisitor.getSubGraph());
        }
    }

    // OK
    @Override
    public void visitPostfixExpression(PsiPostfixExpression expression) {
        PsiExpression operandExpression = expression.getOperand();
        CFGExpressionVisitor operandExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
        operandExpression.accept(operandExpressionVisitor);
        subGraph.merge(operandExpressionVisitor.getSubGraph());
    }

    // TODO
    @Override
    public void visitReferenceExpression(PsiReferenceExpression expression) {
        //TODO: need to improve the implementation
        //TODO: need to handle the case for instance variable, static method, etc.
        //at this moment, only process the case which it has a PsiIdentifier as a child
        PsiElement psiElement = expression.getLastChild();
        if (psiElement instanceof PsiIdentifier) {
            PsiIdentifier lastChild = (PsiIdentifier) psiElement;
//            if (lastChild.getText().equals(CompletionUtilCore.DUMMY_IDENTIFIER_TRIMMED)) {
//                SMethodNode methodNode = new SMethodNode(cfgParameters.idForNewNode(), SMethodSignature.DUMMY_METHOD_SIGNATURE);
//                PsiExpression prevExpression = PsiTreeUtil.getPrevSiblingOfType(lastChild, PsiExpression.class);
//                CFGExpressionVisitor prevExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
//                prevExpression.accept(prevExpressionVisitor);
//                subGraph.merge(prevExpressionVisitor.getSubGraph());
//                SObjectNode currentObjectNode = prevExpressionVisitor.getOutObjectNode();
//                if (currentObjectNode != null) {
//                    currentObjectNode.getDataEdges().add(methodNode);
//                    subGraph.merge(methodNode);
//                    cfgParameters.setEditingObjectNode(currentObjectNode);
//                    cfgParameters.setEditingMethodNode(methodNode);
//                }
//            } else {
                outObjectNode = cfgParameters.getIdentStack().get(lastChild.getText());

//        if ((outObjectNode != null) && (cfgParameters.getVarToEval() != null) && outObjectNode.getIdentifier().equals(cfgParameters.getVarToEval().getText()))
//            cfgParameters.setEvalObjectNode(outObjectNode);
//    }
        }
    }

    // OK
    @Override
    public void visitArrayAccessExpression(PsiArrayAccessExpression expression) {
        PsiExpression indexExpression = expression.getIndexExpression();
        if (indexExpression != null) {
            CFGExpressionVisitor indexExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
            indexExpression.accept(indexExpressionVisitor);
            subGraph.merge(indexExpressionVisitor.getSubGraph());
        }

        PsiExpression arrayExpression = expression.getArrayExpression();
        CFGExpressionVisitor arrayExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
        arrayExpression.accept(arrayExpressionVisitor);
        subGraph.merge(arrayExpressionVisitor.getSubGraph());
    }

    // OK
    @Override
    public void visitMethodCallExpression(PsiMethodCallExpression expression) {

        //SMethodNode methodNode = null;
        // create a method node that represents the method in the expression
        if (expression.resolveMethod() != null) {
            SMethodNode methodNode = new SMethodNode(cfgParameters.idForNewNode(), expression.getTextOffset(), new SMethodSignature(expression.resolveMethod(), expression));

            // process the caller side of the method
            PsiExpression qualifierExpression = expression.getMethodExpression().getQualifierExpression();
            CFGExpressionVisitor callerCFGExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
            if (qualifierExpression != null) {
                qualifierExpression.accept(callerCFGExpressionVisitor);
                subGraph.merge(callerCFGExpressionVisitor.getSubGraph());
            }
            SObjectNode callerOutObjectNode = callerCFGExpressionVisitor.getOutObjectNode();
            if (callerOutObjectNode != null) callerOutObjectNode.getDataEdges().add(methodNode);

            // process parameters of the method
            PsiExpression[] parameterExpressions = expression.getArgumentList().getExpressions();
            for (PsiExpression parameterExpression : parameterExpressions) {
                // for each expression represents an parameters, merge subgraph
                CFGExpressionVisitor paramCFGExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
                if (parameterExpression != null) {
                    parameterExpression.accept(paramCFGExpressionVisitor);
                    subGraph.merge(paramCFGExpressionVisitor.getSubGraph());
                }
                // add data edges from nodes represent parameters to the method node
                SObjectNode paramOutObjectNode = paramCFGExpressionVisitor.getOutObjectNode();
                if (paramOutObjectNode != null) paramOutObjectNode.getDataEdges().add(methodNode);
            }

            //add the method node to the subgraph
//            for (SNode outNode : subGraph.getOuts())
//                outNode.getControlEdges().add(methodNode);
//            subGraph.add(methodNode);
//            if (subGraph.getIn() == null) subGraph.setIn(methodNode);
//            subGraph.getOuts().clear();
//            subGraph.getOuts().add(methodNode);
            subGraph.merge(methodNode);

            //create object node represents return values
            PsiType returnType = expression.resolveMethod().getReturnType();
            if (returnType != null) {
                if (PsiUtils.isObjectType(returnType)) {
                    //outObjectNode = cfgParameters.getIdentStack().get(expression.getText());
                    //if (outObjectNode == null) {
                    outObjectNode = new SObjectNode(cfgParameters.idForNewNode(), expression.getText(), returnType.getCanonicalText(), methodNode.getStartOffset());
                    subGraph.add(outObjectNode);
                    cfgParameters.getIdentStack().put(expression.getText(), outObjectNode);
                    //}
                    methodNode.getDataEdges().add(outObjectNode);
                }
            }
        } else {
            //process the caller side of the method
            CFGExpressionVisitor callerCFGExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
            PsiExpression qualifierExpression = expression.getMethodExpression().getQualifierExpression();
            if (qualifierExpression != null) {
                qualifierExpression.accept(callerCFGExpressionVisitor);
                subGraph.merge(callerCFGExpressionVisitor.getSubGraph());
            }

            //process parameters of the method
            PsiExpression[] parameterExpressions = expression.getArgumentList().getExpressions();
            for (PsiExpression parameterExpression : parameterExpressions) {
                //for each expression represents an parameters, merge subgraph
                CFGExpressionVisitor paramCFGExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
                if (parameterExpression != null) {
                    parameterExpression.accept(paramCFGExpressionVisitor);
                    subGraph.merge(paramCFGExpressionVisitor.getSubGraph());
                }
            }
        }

    }


    // OK
    @Override
    public void visitNewExpression(PsiNewExpression expression) {
        // create a method node represents the constructor
        // and correct the return type because contructors return null
        if (expression.resolveMethod() != null) {
            SMethodNode methodNode = new SMethodNode(cfgParameters.idForNewNode(), expression.getTextOffset(), new SMethodSignature(expression.resolveMethod(), expression));
            methodNode.getSignature().setReturnType(methodNode.getSignature().getContainingClass());
            // process the parameters of the method
            if (expression.getArgumentList() != null) {
                PsiExpression[] parameterExpressions = expression.getArgumentList().getExpressions();
                for (PsiExpression parameterExpression : parameterExpressions) {
                    CFGExpressionVisitor paramExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
                    if (parameterExpression != null) {
                        parameterExpression.accept(paramExpressionVisitor);
                        subGraph.merge(paramExpressionVisitor.getSubGraph());
                    }
                    // add data edges from nodes represent parameters to the method node
                    SObjectNode paramOutObjectNode = paramExpressionVisitor.getOutObjectNode();
                    if (paramOutObjectNode != null) paramOutObjectNode.getDataEdges().add(methodNode);
                }
            }

            //add the method node to the subgraph
//            for (SNode outNode: subGraph.getOuts())
//                outNode.getControlEdges().add(methodNode);
//            subGraph.add(methodNode);
//            if (subGraph.getIn() == null) subGraph.setIn(methodNode);
//            subGraph.getOuts().clear(); //just for defending
//            subGraph.getOuts().add(methodNode);
            subGraph.merge(methodNode);

            // create an object node represents return values
            PsiType returnType = expression.getType();
            if (returnType != null) {
                if (PsiUtils.isObjectType(returnType)) {
                    //outObjectNode = cfgParameters.getIdentStack().get(expression.getText());
                    //if (outObjectNode == null) {
                    outObjectNode = new SObjectNode(cfgParameters.idForNewNode(), expression.getText(), returnType.getCanonicalText(), methodNode.getStartOffset());
                    subGraph.add(outObjectNode);
                    cfgParameters.getIdentStack().put(expression.getText(), outObjectNode);
                    //}
                    methodNode.getDataEdges().add(outObjectNode);
                }
            }
        } else {
            //process the parameters of the method
            if (expression.getArgumentList() != null) {
                PsiExpression[] parameterExpressions = expression.getArgumentList().getExpressions();
                for (PsiExpression parameterExpression: parameterExpressions) {
                    if (parameterExpression != null) {
                        CFGExpressionVisitor paramExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
                        parameterExpression.accept(paramExpressionVisitor);
                        subGraph.merge(paramExpressionVisitor.getSubGraph());
                    }
                }
            }
        }

    }


    // OK
    @Override
    public void visitArrayInitializerExpression(PsiArrayInitializerExpression expression) {
        PsiExpression[] initializerExpressions = expression.getInitializers();
        for (PsiExpression initializerExpression : initializerExpressions) {
            if (initializerExpression != null) {
                CFGExpressionVisitor initializerExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
                initializerExpression.accept(initializerExpressionVisitor);
                subGraph.merge(initializerExpressionVisitor.getSubGraph());
            }
        }
    }

    // OK
    @Override
    public void visitSuperExpression(PsiSuperExpression expression) {
        //don't process this expression
    }

    // OK
    @Override
    public void visitThisExpression(PsiThisExpression expression) {
        //don't process this expression
    }

    // OK
    @Override
    public void visitInstanceOfExpression(PsiInstanceOfExpression expression) {
        PsiExpression operandExpression = expression.getOperand();
        CFGExpressionVisitor operandExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
        operandExpression.accept(operandExpressionVisitor);
        subGraph.merge(operandExpressionVisitor.getSubGraph());
    }

    // OK
    @Override
    public void visitLiteralExpression(PsiLiteralExpression expression) {
        //don't process this expression
    }

    // OK
    @Override
    public void visitAssignmentExpression(PsiAssignmentExpression expression) {
        PsiExpression rightExpression = expression.getRExpression();
        CFGExpressionVisitor rightCfgExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
        if (rightExpression != null) {
            rightExpression.accept(rightCfgExpressionVisitor);
            subGraph.merge(rightCfgExpressionVisitor.getSubGraph());
        }

        PsiExpression leftExpression = expression.getLExpression();
        CFGExpressionVisitor leftCfgExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
        leftExpression.accept(leftCfgExpressionVisitor);

        if (leftCfgExpressionVisitor.getOutObjectNode() != null) {
            SObjectNode objectNode = new SObjectNode(cfgParameters.idForNewNode(), leftCfgExpressionVisitor.getOutObjectNode().getIdentifier(), leftCfgExpressionVisitor.getOutObjectNode().getType(), leftCfgExpressionVisitor.getOutObjectNode().getOffset());
            subGraph.add(objectNode);
            cfgParameters.getIdentStack().put(objectNode.getIdentifier(), objectNode);
            if (PsiUtils.canAddDataEdgesBack(rightExpression)) {
                if (rightCfgExpressionVisitor.getSubGraph().getOuts().size() > 0) {
                    if (rightCfgExpressionVisitor.getSubGraph().getOuts().get(0) instanceof SMethodNode) {
                        SMethodNode lastMethodNode = (SMethodNode) rightCfgExpressionVisitor.getSubGraph().getOuts().get(0);
                        lastMethodNode.getDataEdges().clear();
                        lastMethodNode.getDataEdges().add(objectNode);
                    }
                }
            }
        }

    }

    // OK
    @Override
    public void visitLambdaExpression(PsiLambdaExpression expression) {
        // don't process this expression
    }

    // OK
    @Override
    public void visitMethodReferenceExpression(PsiMethodReferenceExpression expression) {
        // don't process this expression
    }

    // OK
    @Override
    public void visitClassObjectAccessExpression(PsiClassObjectAccessExpression expression) {
        // don't process this expression
    }

    // OK
    @Override
    public void visitConditionalExpression(PsiConditionalExpression expression) {
        // this is: cond ? expr true : expr false
        SourceCodeCFG conditionalSubGraph = new SourceCodeCFG();

        PsiExpression conditionalExpression = expression.getCondition();
        CFGExpressionVisitor conditionalExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
        conditionalExpression.accept(conditionalExpressionVisitor);
        conditionalSubGraph.merge(conditionalExpressionVisitor.getSubGraph());

        SControlNode controlNode = new SControlNode(cfgParameters.idForNewNode(), SControlNode.IF_LABEL);
        conditionalSubGraph.merge(controlNode);

        // then expression is @Nullable
        PsiExpression thenExpression = expression.getThenExpression();
        CFGExpressionVisitor thenExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
        if (thenExpression != null) {
            thenExpression.accept(thenExpressionVisitor);
        }

        // +else expression is @Nullable
        PsiExpression elseExpression = expression.getElseExpression();
        CFGExpressionVisitor elseExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
        if (elseExpression != null) {
            elseExpression.accept(elseExpressionVisitor);
        }

        if (thenExpressionVisitor.getSubGraph().getOuts().size() > 0) {
            if (elseExpressionVisitor.getSubGraph().getOuts().size() > 0) {
                conditionalSubGraph.merge(thenExpressionVisitor.getSubGraph());
                conditionalSubGraph.getOuts().clear();
                conditionalSubGraph.getOuts().add(controlNode);
                conditionalSubGraph.merge(elseExpressionVisitor.getSubGraph());
                conditionalSubGraph.getOuts().addAll(thenExpressionVisitor.getSubGraph().getOuts());
            } else {
                conditionalSubGraph.merge(thenExpressionVisitor.getSubGraph());
                conditionalSubGraph.getOuts().add(controlNode);
            }
        } else {
            if (elseExpressionVisitor.getSubGraph().getOuts().size() > 0) {
                conditionalSubGraph.merge(elseExpressionVisitor.getSubGraph());
                conditionalSubGraph.getOuts().add(controlNode);
            }
        }
        subGraph.merge(conditionalSubGraph);
    }

    // OK
    @Override
    public void visitParenthesizedExpression(PsiParenthesizedExpression expression) {
        PsiExpression insideExpression = expression.getExpression();
        if (insideExpression != null) {
            CFGExpressionVisitor insideExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
            insideExpression.accept(insideExpressionVisitor);
            subGraph.merge(insideExpressionVisitor.getSubGraph());
        }
    }

    // OK
    @Override
    public void visitPrefixExpression(PsiPrefixExpression expression) {
        PsiExpression operandExpression = expression.getOperand();
        if (operandExpression != null) {
            CFGExpressionVisitor expressionVisitor = new CFGExpressionVisitor(cfgParameters);
            operandExpression.accept(expressionVisitor);
            subGraph.merge(expressionVisitor.getSubGraph());
        }
    }

    // OK
    @Override
    public void visitTypeCastExpression(PsiTypeCastExpression expression) {
        PsiExpression operandExpression = expression.getOperand();
        if (operandExpression != null) {
            CFGExpressionVisitor expressionVisitor = new CFGExpressionVisitor(cfgParameters);
            operandExpression.accept(expressionVisitor);
            subGraph.merge(expressionVisitor.getSubGraph());
        }
    }

    public SObjectNode getOutObjectNode() {
        return outObjectNode;
    }

    public void setOutObjectNode(SObjectNode outObjectNode) {
        this.outObjectNode = outObjectNode;
    }
}
