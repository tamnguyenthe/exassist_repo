package usu.seal.salad.cfg.node;

/**
 * Created by Tam Nguyen on 6/11/2015.
 */
public class SObjectNode extends SNode {
    private String type;
    private String identifier;
    private int offset;

    public SObjectNode(int id, String identifier, String type, int offset) {
        super(id);
        this.identifier = identifier;
        this.type = type;
        this.offset = offset;
    }

    @Override
    public String toString() {
        return "SObjectNode{" +
                "id=" + id +
                ", type=" + type +
                ", identifier=" + identifier +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        if (!(obj instanceof SObjectNode)) return false;
        SObjectNode objectNode = (SObjectNode)obj;
        return this.getId() == objectNode.getId() &&
                this.getType().equals(objectNode.getType()) &&
                this.getIdentifier().equals(objectNode.getIdentifier());
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }
}
