package usu.seal.salad.cfg.node;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Tam Nguyen on 6/11/2015.
 */
public class SNode implements Comparable<SNode> {
    protected int id;
    protected Set<SNode> controlEdges;
    protected Set<SNode> dataEdges;
    protected Set<SNode> dataBackEdges;  //used for extracting data dependency

    protected SNode(int id) {
        this.id = id;
        this.controlEdges = new TreeSet<>();
        this.dataEdges = new TreeSet<>();
        this.dataBackEdges = new TreeSet<>();
    }

    public Set<SNode> getRelatedDataNodes() {
        Set<SNode> resultSet = new HashSet<>();
        resultSet.addAll(getDataEdges());
        resultSet.addAll(getDataBackEdges());
        return resultSet;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public int compareTo(SNode o) {
        return Integer.compare(getId(), o.getId());
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Set<SNode> getControlEdges() {
        return controlEdges;
    }

    public void setControlEdges(Set<SNode> controlEdges) {
        this.controlEdges = controlEdges;
    }

    public Set<SNode> getDataEdges() {
        return dataEdges;
    }

    public void setDataEdges(Set<SNode> dataEdges) {
        this.dataEdges = dataEdges;
    }

    public Set<SNode> getDataBackEdges() {
        return dataBackEdges;
    }

    public void setDataBackEdges(Set<SNode> dataBackEdges) {
        this.dataBackEdges = dataBackEdges;
    }
}
