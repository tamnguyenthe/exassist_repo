package usu.seal.salad.cfg.node;

/**
 * Created by Tam Nguyen on 6/11/2015.
 */
public class SMethodNode extends SNode {

    private SMethodSignature signature;
    private int startOffset;

    public SMethodNode(int id, int offset, SMethodSignature signature) {
        super(id);
        this.signature = signature;
        this.startOffset = offset;
    }

    public SMethodSignature getSignature() {
        return signature;
    }

    public void setSignature(SMethodSignature signature) {
        this.signature = signature;
    }

    @Override
    public String toString() {
        return "SMethodNode{" +
                "id=" + id +
                ", signature=" + signature +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        if (!(obj instanceof SMethodNode)) return false;
        SMethodNode methodNode = (SMethodNode)obj;
        return this.getId() == methodNode.getId() &&
                this.getSignature().equals(methodNode.getSignature());
    }

    public int getStartOffset() {
        return startOffset;
    }
}

