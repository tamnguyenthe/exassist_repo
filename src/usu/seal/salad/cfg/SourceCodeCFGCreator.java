package usu.seal.salad.cfg;

import com.intellij.psi.*;
import com.intellij.psi.util.PsiTreeUtil;
import usu.seal.salad.cfg.node.SControlNode;
import usu.seal.salad.cfg.node.SNode;
import usu.seal.salad.cfg.node.SObjectNode;
import usu.seal.salad.cfg.visitor.SourceCodeCFGVisitor;
import usu.seal.salad.util.PsiUtils;

//import usu.seal.salad.completion.CompletionQuery;

/**
 * Created by Student on 6/11/2015.
 */
public class SourceCodeCFGCreator {
    private PsiMethod psiMethod;
    private CFGParameters cfgParameters;

    public SourceCodeCFGCreator(PsiMethod psiMethod) {
        this.psiMethod = psiMethod;
        cfgParameters = new CFGParameters();
    }

    public void buildCFG() {
        initializeCFG();
        createCFG();
    }

    private void initializeCFG() {
        //add fields
        cfgParameters.getIdentStack().pushANewMap();
        PsiClass psiClass = PsiTreeUtil.getParentOfType(psiMethod, PsiClass.class);
        PsiField[] allFields = psiClass.getAllFields();
        if (PsiUtils.isStaticMethod(psiMethod)) {
            for (PsiField psiField : allFields) {
                if (!PsiUtils.isStaticField(psiField)) continue;
                if (PsiUtils.isObjectType(psiField.getType())) {
                    SObjectNode objectNode = new SObjectNode(cfgParameters.idForNewNode(),
                                                            psiField.getNameIdentifier().getText(),
                                                            psiField.getType().getCanonicalText(),
                                                            0);
                    cfgParameters.getCfg().add(objectNode);
                    cfgParameters.getIdentStack().put(objectNode.getIdentifier(), objectNode);
                }
            }
        } else {
            for (PsiField psiField : allFields) {
                if (PsiUtils.isStaticField(psiField)) continue;
                if (PsiUtils.isObjectType(psiField.getType())) {
                    SObjectNode objectNode = new SObjectNode(cfgParameters.idForNewNode(),
                            psiField.getNameIdentifier().getText(),
                            psiField.getType().getCanonicalText(),
                            0);
                    cfgParameters.getCfg().add(objectNode);
                    cfgParameters.getIdentStack().put(objectNode.getIdentifier(), objectNode);
                }
            }
        }
        //add parameters
        cfgParameters.getIdentStack().pushANewMap();
        PsiParameterList psiParameterList = PsiTreeUtil.getChildOfType(psiMethod, PsiParameterList.class);
        if (psiParameterList != null) {
            if (PsiTreeUtil.getChildrenOfType(psiParameterList, PsiParameter.class) != null) {
                for (PsiParameter psiParameter : PsiTreeUtil.getChildrenOfType(psiParameterList, PsiParameter.class)) {
                    if (PsiUtils.isObjectType(psiParameter.getType())) {
                        SObjectNode objectNode = new SObjectNode(cfgParameters.idForNewNode(),
                                psiParameter.getNameIdentifier().getText(),
                                psiParameter.getType().getCanonicalText(),
                                0);
                        cfgParameters.getCfg().add(objectNode);
                        cfgParameters.getIdentStack().put(objectNode.getIdentifier(), objectNode);
                    }
                }
            }
        }
    }

    private void createCFG() {
        cfgParameters.getIdentStack().pushANewMap();
        PsiCodeBlock psiCodeBlock = PsiTreeUtil.getChildOfType(psiMethod, PsiCodeBlock.class);
        SourceCodeCFGVisitor sourceCodeCFGVisitor = new SourceCodeCFGVisitor(cfgParameters);
        if (psiCodeBlock != null) {
            psiCodeBlock.accept(sourceCodeCFGVisitor);
            cfgParameters.getCfg().merge(sourceCodeCFGVisitor.getSubGraph());
            cfgParameters.getCfg().addDataBackEdges();
            for (SNode node : cfgParameters.getCfg().getOuts()) {
                SControlNode controlNode = new SControlNode(cfgParameters.idForNewNode(), SControlNode.RETURN_LABEL);
                node.getControlEdges().add(controlNode);
            }
            for (SNode sNode : cfgParameters.getCfg().getNodes()) {
                if (sNode instanceof SControlNode) {
                    SControlNode controlNode = (SControlNode)sNode;
                    if (controlNode.getLabel().equals(SControlNode.RETURN_LABEL)) {
                        controlNode.getControlEdges().clear();
                    }
                }
            }
        }
    }


    public PsiMethod getPsiMethod() {
        return psiMethod;
    }

    public void setPsiMethod(PsiMethod psiMethod) {
        this.psiMethod = psiMethod;
    }

    public CFGParameters getCfgParameters() {
        return cfgParameters;
    }

    public void setCfgParameters(CFGParameters cfgParameters) {
        this.cfgParameters = cfgParameters;
    }
}
